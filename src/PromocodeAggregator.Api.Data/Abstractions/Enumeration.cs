﻿using System.Collections.Immutable;
using System.Reflection;

// ReSharper disable UnusedMember.Global

namespace PromocodeAggregator.Api.Data.Abstractions;

public class Enumeration<TKey, TEnumeration> : IEquatable<Enumeration<TKey, TEnumeration>>
	where TEnumeration : Enumeration<TKey, TEnumeration>
{
	private static ImmutableDictionary<TKey, TEnumeration> _cachedAllValues;

	public Enumeration(TKey id, string name)
	{
		Id = id;
		Name = name;
	}

	public TKey Id { get; private set; }

	public string Name { get; private set; }

	public static TEnumeration GetById(TKey id)
	{
		_cachedAllValues ??= GetAllValues()
			.ToImmutableDictionary(x => x.Id, x => x);

		return _cachedAllValues[id];
	}

	public static List<TEnumeration> GetAll()
	{
		_cachedAllValues ??= GetAllValues()
			.ToImmutableDictionary(x => x.Id, x => x);

		return _cachedAllValues.Values.ToList();
	}

	private static IEnumerable<TEnumeration> GetAllValues()
	{
		var type = typeof(TEnumeration);

		return type.GetFields(BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.Public)
			.Where(x => x.FieldType == type)
			.Select(x => (TEnumeration)x.GetValue(null));
	}

	public bool Equals(Enumeration<TKey, TEnumeration> other)
	{
		if (ReferenceEquals(null, other)) return false;
		if (ReferenceEquals(this, other)) return true;

		return EqualityComparer<TKey>.Default.Equals(Id, other.Id) && Name == other.Name;
	}

	public override bool Equals(object obj)
	{
		if (ReferenceEquals(null, obj))
		{
			return false;
		}

		if (ReferenceEquals(this, obj))
		{
			return true;
		}

		if (obj.GetType() != GetType())
		{
			return false;
		}

		return Equals((Enumeration<TKey, TEnumeration>)obj);
	}

	public override int GetHashCode() =>
		HashCode.Combine(Id, Name);

	public static bool operator ==(Enumeration<TKey, TEnumeration> left, Enumeration<TKey, TEnumeration> right) =>
		Equals(left, right);

	public static bool operator !=(Enumeration<TKey, TEnumeration> left, Enumeration<TKey, TEnumeration> right) =>
		!Equals(left, right);
}
