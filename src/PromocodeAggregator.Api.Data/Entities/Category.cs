﻿namespace PromocodeAggregator.Api.Data.Entities;

public class Category : BaseEntity
{
	public Category(string name, string description, string source) : this()
	{
		Name = name;
		Description = description;
		Source = source;
	}

	public Category()
	{
		Id = Guid.NewGuid();
	}

	public Guid Id { get; private set; }

	public string Name { get; private set; }

	public string Description { get; private set; }

	public string Source { get; private set; }
}
