﻿using PromocodeAggregator.Api.Data.Abstractions;

namespace PromocodeAggregator.Api.Data.Entities;

public class PropositionType : Enumeration<string, PropositionType>
{
	public static PropositionType Promocode = new ("Promocode", "Промокод");
	
	public static PropositionType Promotion = new ("Promotion", "Акция");

	public PropositionType(string id, string name) : base(id, name)
	{
	}
}
