﻿// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable CollectionNeverUpdated.Global

namespace PromocodeAggregator.Api.Data.Entities;

public class Company : BaseEntity
{
	private Company()
	{
		Id = Guid.NewGuid();
	}

	public Company(string name, string source, string imageBase64, string imageLink, Category category) : this()
	{
		Name = name;
		Source = source;
		ImageBase64 = imageBase64;
		ImageLink = imageLink;
		Category = category;
	}

	public Guid Id { get; private set; }

	public string Name { get; private set; }

	public string Source { get; private set; }

	public string ImageBase64 { get; private set; }

	public string ImageLink { get; private set; }

	public Guid CategoryId { get; private set; }

	public long SearchCount { get; private set; }

	public Category Category { get; private set; }

	public List<Proposition> Propositions { get; private set; } = new();

	public void IncSearchesCount() => SearchCount++;

	public void AddPropositions(IEnumerable<Proposition> propositions) => Propositions.AddRange(propositions);
}
