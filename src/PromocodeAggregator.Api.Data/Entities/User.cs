﻿namespace PromocodeAggregator.Api.Data.Entities;

public class User : BaseEntity
{
	public User(Guid id) : this()
	{
		Id = id;
	}

	private User()
	{
	}

	public Guid Id { get; private set; }

	public List<Proposition> FavouriteProposition { get; private set; } = new();

	public List<Company> FavouriteCompanies { get; private set; } = new();

	public void AddFavouriteProposition(Proposition proposition)
	{
		if (FavouriteProposition.All(x => x.Id != proposition.Id))
		{
			FavouriteProposition.Add(proposition);
		}
	}

	public void AddFavouriteCompany(Company company)
	{
		if (FavouriteCompanies.All(x => x.Id != company.Id))
		{
			FavouriteCompanies.Add(company);
		}
	}

	public void RemoveFavouriteProposition(Proposition proposition) =>
		FavouriteProposition = FavouriteProposition.Where(x => x.Id != proposition.Id).ToList();

	public void RemoveFavouriteCompany(Company company) =>
		FavouriteCompanies = FavouriteCompanies.Where(x => x.Id != company.Id).ToList();
}
