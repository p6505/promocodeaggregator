﻿// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace PromocodeAggregator.Api.Data.Entities;

public class Proposition : BaseEntity
{
	public Proposition(
		PropositionType propositionType,
		string propositionValue,
		DateTimeOffset? expiration,
		string name,
		string description,
		string sourceUrl)
		: this()
	{
		PropositionTypeId = propositionType.Id;
		PropositionValue = propositionValue;
		Expiration = expiration;
		Name = name;
		Description = description;
		SourceUrl = sourceUrl;
	}

	private Proposition()
	{
		Id = Guid.NewGuid();
	}

	public Guid Id { get; private set; }

	public string PropositionValue { get; private set; }

	public DateTimeOffset? Expiration { get; private set; }

	public string Name { get; private set; }

	public string Description { get; private set; }

	public string SourceUrl { get; private set; }

	public string PropositionTypeId { get; private set; }

	public long SearchCount { get; private set; }

	public PropositionType PropositionTypeValue => PropositionType.GetById(PropositionTypeId);

	public Guid CompanyId { get; private set; }

	public Company Company { get; private set; }

	public List<User> Users { get; private set; }

	public long IncSearchesCount() => SearchCount++;
}
