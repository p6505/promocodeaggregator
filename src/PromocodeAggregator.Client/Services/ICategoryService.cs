﻿using PromocodeAggregator.Client.Models;
using RestEase;

namespace PromocodeAggregator.Client.Services;

public interface ICategoryService
{
	[Get("api/categories")]
	Task<CollectionModel<CategoryModel>> GetCategoriesList();

	[Get("api/categories/{id}")]
	Task<CategoryModel> GetCategoryById([Path] Guid id);
}
