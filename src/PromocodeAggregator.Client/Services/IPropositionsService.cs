﻿using PromocodeAggregator.Client.Models;
using PromocodeAggregator.Client.Models.Propositions;
using RestEase;

namespace PromocodeAggregator.Client.Services;

public interface IPropositionsService
{
	[Get("api/propositions")]
	Task<CollectionModel<PropositionModel>> GetPropositions(
		[Query] Guid? companyId,
		[Query] string query,
		[Query] int skip,
		[Query] int take);

	[Get("api/propositions/{id}")]
	Task<PropositionDetailsModel> GetProposition([Path] Guid id);

	[Post("api/propositions")]
	Task<PropositionModel> AddProposition([Body] AddPropositionRequestModel requestModel);

	[Get("api/propositions/favourite")]
	Task<CollectionModel<PropositionModel>> GetFavouritePropositions();

	[Post("api/propositions/{id}/favourite")]
	Task AddPropositionToFavourite([Path] Guid id);

	[Delete("api/propositions/{id}/favourite")]
	Task RemovePropositionFromFavourite([Path] Guid id);

	[Get("api/propositions/most-popular")]
	Task<CollectionModel<PropositionModel>> GetMostPopularPropositions(
		[Query] int skip,
		[Query] int take);
}
