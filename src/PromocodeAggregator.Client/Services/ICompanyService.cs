﻿using PromocodeAggregator.Client.Models;
using PromocodeAggregator.Client.Models.Companies;
using RestEase;

namespace PromocodeAggregator.Client.Services;

[SerializationMethods(Query = QuerySerializationMethod.Serialized)]
public interface ICompanyService
{
	[Get("api/companies")]
	Task<CollectionModel<CompanyModel>> GetCompaniesList(
		[Query] string query,
		[Query] Guid? categoryId,
		[Query] int skip,
		[Query] int take);

	[Post("api/companies")]
	Task AddCompany([Body] AddCompanyRequestModel requestModel);

	[Get("api/companies/most-popular")]
	Task<CollectionModel<CompanyStatisticModel>> GetMostPopularCompanies(
		[Query] int skip,
		[Query] int take);

	[Get("api/companies/favourite")]
	Task<CollectionModel<CompanyModel>> GetFavouriteCompanies();

	[Post("api/companies/{id}/favourite")]
	Task AddCompanyToFavourite([Path] Guid id);

	[Delete("api/companies/{id}/favourite")]
	Task RemoveCompanyFromFavourite([Path] Guid id);
}
