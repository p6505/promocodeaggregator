﻿namespace PromocodeAggregator.Client.Models;

public class PaginationModel
{
	/// <summary>
	/// Количества записей для пропуска
	/// </summary>
	public int Skip { get; init; }

	/// <summary>
	/// Количество записей, которое нужно взять
	/// </summary>
	public int Take { get; init; } = 20;
}
