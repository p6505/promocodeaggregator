﻿// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global

namespace PromocodeAggregator.Client.Models;

public class CollectionModel<T> where T : class
{
	public CollectionModel() : this(Array.Empty<T>())
	{
	}

	[Newtonsoft.Json.JsonConstructor]
	[System.Text.Json.Serialization.JsonConstructor]
	public CollectionModel(ICollection<T> items)
	{
		Items = items;
	}

	public int ReturnedItemsLength => Items.Count;

	public ICollection<T> Items { get; }
}
