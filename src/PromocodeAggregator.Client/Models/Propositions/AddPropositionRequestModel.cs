﻿namespace PromocodeAggregator.Client.Models.Propositions;

public class AddPropositionRequestModel
{
	public Guid CompanyId { get; set; }

	public string Value { get; set; }

	public DateTimeOffset? Expiration { get; set; }

	public string Name { get; set; }

	public string Description { get; set; }

	public string SourceUrl { get; set; }
}
