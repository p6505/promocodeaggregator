using PromocodeAggregator.Client.Models.Companies;

namespace PromocodeAggregator.Client.Models.Propositions;

public class PropositionDetailsModel : PropositionModel
{
	public string Code { get; set; }

	public DateTimeOffset? Expiration { get; set; }

	public CompanyModel Company { get; set; }
}
