﻿namespace PromocodeAggregator.Client.Models.Propositions;

public class GetPropositionListRequestModel : PaginationModel
{
	public Guid? CompanyId { get; set; }

	public string Query { get; set; }
}
