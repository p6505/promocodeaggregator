﻿namespace PromocodeAggregator.Client.Models.Propositions;

public class PropositionModel
{
	public Guid Id { get; set; }

	public string Name { get; set; }

	public string Description { get; set; }
}
