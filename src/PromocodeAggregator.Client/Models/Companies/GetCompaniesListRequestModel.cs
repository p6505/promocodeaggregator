﻿namespace PromocodeAggregator.Client.Models.Companies;

public class GetCompaniesListRequestModel : PaginationModel
{
	/// <summary>
	/// Фильтр по названию
	/// </summary>
	public string Query { get; init; }

	/// <summary>
	/// Фильтр по категории
	/// </summary>
	public Guid? CategoryId { get; init; }
}
