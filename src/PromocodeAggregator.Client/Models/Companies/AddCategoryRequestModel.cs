namespace PromocodeAggregator.Client.Models.Companies;

public class AddCategoryRequestModel
{
	public string Name { get; set; }

	public string Description { get; set; }

	public string Source { get; set; }
}
