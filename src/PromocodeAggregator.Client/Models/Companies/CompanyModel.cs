﻿namespace PromocodeAggregator.Client.Models.Companies;

public class CompanyModel
{
	public Guid Id { get; set; }

	public string Name { get; set; }

	public string Source { get; set; }
}
