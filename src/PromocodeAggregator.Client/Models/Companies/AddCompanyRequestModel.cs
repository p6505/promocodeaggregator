﻿using PromocodeAggregator.Client.Models.Propositions;

namespace PromocodeAggregator.Client.Models.Companies;

public class AddCompanyRequestModel
{
	public string Name { get; set; }

	public string Source { get; set; }

	public string ImageLink { get; set; }

	public string ImageBase64 { get; set; }

	public AddCategoryRequestModel Category { get; set; }

	public List<AddPropositionRequestModel> Propositions { get; set; }
}
