﻿using PromocodeAggregator.Client.Services;

namespace PromocodeAggregator.Client;

public interface IPropositionsAggregatorClient :
	ICompanyService,
	IPropositionsService,
	ICategoryService
{
}
