﻿using Microsoft.EntityFrameworkCore;
using PromocodeAggregator.Api.Data.Entities;

// ReSharper disable VirtualMemberCallInConstructor

namespace PromocodeAggregator.Api.Infrastructure;

public class DataContext : DbContext
{
	public DataContext(DbContextOptions<DataContext> options) : base(options)
	{
		ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.TrackAll;
	}

	public DbSet<Company> Companies { get; set; }

	public DbSet<Proposition> Propositions { get; set; }

	public DbSet<PropositionType> PropositionTypes { get; set; }

	public DbSet<User> Users { get; set; }

	public DbSet<Category> Categories { get; set; }

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		modelBuilder.ApplyConfigurationsFromAssembly(typeof(DataContext).Assembly);

		base.OnModelCreating(modelBuilder);
	}
}
