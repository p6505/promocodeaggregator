﻿using PromocodeAggregator.Api.Data.Entities;

namespace PromocodeAggregator.Api.Infrastructure.Abstractions;

public interface IRepository
{
	Task<Company> FindCompanyByName(string name, CancellationToken cancellationToken);

	Task<Company> FindOrAddCompany(
		string requestCompanyName,
		string source,
		string imageLink,
		string imageBase64,
		Category category);

	Task<Company> FindCompanyById(Guid id, CancellationToken cancellationToken);

	Task<Category> FindOrAddCategory(string name, string description, string source);

	Task<List<Company>> GetCompanies(
		string query,
		Guid? categoryId,
		int skip,
		int take,
		CancellationToken cancellationToken);

	Task<Category> GetCategoryById(Guid id, CancellationToken cancellationToken);

	Task<List<Category>> GetCategories(string query, CancellationToken cancellationToken);

	Task<Proposition> GetPropositionById(Guid id, CancellationToken cancellationToken);

	Task<List<Proposition>> GetPropositionsForCompany(
		Guid companyId,
		string query,
		int skip,
		int take,
		CancellationToken cancellationToken);

	Task<List<Proposition>> GetPropositionByQuery(
		string query,
		int skip,
		int take,
		CancellationToken cancellationToken);

	Task AddOrUpdateProposition(Company company, List<Proposition> propositions);

	Task<User> GetOrAddUser(Guid userId, CancellationToken cancellationToken);

	Task<User> FindUser(Guid userId, CancellationToken cancellationToken);

	Task<List<Company>> GetMostPopularCompanies(int skip, int limit, CancellationToken cancellationToken);

	Task<List<Proposition>> GetMostPopularPropositions(int skip, int limit, CancellationToken cancellationToken);

	Task SaveChanges(CancellationToken cancellationToken);
}
