﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PromocodeAggregator.Api.Data.Entities;

namespace PromocodeAggregator.Api.Infrastructure.Configurations;

public class CompanyConfiguration : BaseEntityConfiguration<Company>
{
	public override void Configure(EntityTypeBuilder<Company> builder)
	{
		builder.ToTable(nameof(Company));

		builder.HasKey(x => x.Id);

		builder
			.Property(x => x.Name)
			.IsRequired();

		builder
			.HasMany(x => x.Propositions)
			.WithOne(x => x.Company)
			.HasForeignKey(x => x.CompanyId);

		builder.HasOne(x => x.Category)
			.WithMany()
			.HasForeignKey(x => x.CategoryId);

		builder.HasIndex(x => x.Name).IsUnique();

		base.Configure(builder);
	}
}
