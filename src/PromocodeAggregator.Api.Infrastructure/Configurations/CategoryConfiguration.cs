﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PromocodeAggregator.Api.Data.Entities;

namespace PromocodeAggregator.Api.Infrastructure.Configurations;

public class CategoryConfiguration : IEntityTypeConfiguration<Category>
{
	public void Configure(EntityTypeBuilder<Category> builder)
	{
		builder.ToTable(nameof(Category));

		builder.Property(x => x.Name).IsRequired();

		builder.HasIndex(x => x.Name).IsUnique();
		builder.HasIndex(x => x.Description).IsUnique();
	}
}
