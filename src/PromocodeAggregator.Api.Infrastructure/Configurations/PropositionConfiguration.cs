﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PromocodeAggregator.Api.Data.Entities;

namespace PromocodeAggregator.Api.Infrastructure.Configurations;

public class PropositionConfiguration : BaseEntityConfiguration<Proposition>
{
	public override void Configure(EntityTypeBuilder<Proposition> builder)
	{
		builder.ToTable(nameof(Proposition));

		builder.HasKey(x => x.Id);

		builder
			.Property(x => x.PropositionValue)
			.IsRequired();

		builder
			.Property(x => x.SourceUrl)
			.IsRequired();

		builder
			.HasOne<PropositionType>()
			.WithMany()
			.HasForeignKey(x => x.PropositionTypeId);

		builder.HasIndex(x => new
		{
			x.PropositionValue,
			x.CompanyId,
			x.Name
		}).IsUnique();
		builder.HasIndex(x => x.Expiration);

		base.Configure(builder);
	}
}
