﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PromocodeAggregator.Api.Data.Entities;

namespace PromocodeAggregator.Api.Infrastructure.Configurations;

public class PropositionTypeConfiguration : IEntityTypeConfiguration<PropositionType>
{
	public void Configure(EntityTypeBuilder<PropositionType> builder)
	{
		builder.ToTable(nameof(PropositionType));

		builder.HasKey(x => x.Id);

		builder.HasIndex(x => x.Name).IsUnique();

		builder.HasData(PropositionType.GetAll());
	}
}
