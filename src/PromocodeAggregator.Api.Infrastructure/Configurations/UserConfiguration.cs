﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PromocodeAggregator.Api.Data.Entities;

namespace PromocodeAggregator.Api.Infrastructure.Configurations;

public class UserConfiguration : BaseEntityConfiguration<User>
{
	public override void Configure(EntityTypeBuilder<User> builder)
	{
		builder.ToTable(nameof(User));

		builder.HasKey(x => x.Id);

		builder
			.HasMany(x => x.FavouriteProposition)
			.WithMany(x => x.Users)
			.UsingEntity("FavouriteProposition");

		builder
			.HasMany(x => x.FavouriteCompanies)
			.WithMany()
			.UsingEntity("FavouriteCompany");

		base.Configure(builder);
	}
}
