﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PromocodeAggregator.Api.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddStatistics : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
	        migrationBuilder.InsertData(
		        table: "PropositionType",
		        columns: new[] { "Id", "Name" },
		        values: new object[,]
		        {
			        { "Promocode", "Промокод" },
			        { "Promotion", "Акция" }
		        });
	        
            migrationBuilder.AddColumn<long>(
                name: "SearchCount",
                table: "Proposition",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "SearchCount",
                table: "Company",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Proposition_PropositionTypeId",
                table: "Proposition",
                column: "PropositionTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Proposition_PropositionType_PropositionTypeId",
                table: "Proposition",
                column: "PropositionTypeId",
                principalTable: "PropositionType",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
	        migrationBuilder.DeleteData(
		        table: "PropositionType",
		        keyColumn: "Id",
		        keyValue: "Promocode");

	        migrationBuilder.DeleteData(
		        table: "PropositionType",
		        keyColumn: "Id",
		        keyValue: "Promotion");
	        
            migrationBuilder.DropForeignKey(
                name: "FK_Proposition_PropositionType_PropositionTypeId",
                table: "Proposition");

            migrationBuilder.DropIndex(
                name: "IX_Proposition_PropositionTypeId",
                table: "Proposition");

            migrationBuilder.DropColumn(
                name: "SearchCount",
                table: "Proposition");

            migrationBuilder.DropColumn(
                name: "SearchCount",
                table: "Company");
        }
    }
}
