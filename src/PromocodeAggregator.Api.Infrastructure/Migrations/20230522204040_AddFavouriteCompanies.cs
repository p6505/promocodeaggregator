﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PromocodeAggregator.Api.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddFavouriteCompanies : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FavouriteCompanies",
                columns: table => new
                {
                    FavouriteCompaniesId = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FavouriteCompanies", x => new { x.FavouriteCompaniesId, x.UserId });
                    table.ForeignKey(
                        name: "FK_FavouriteCompanies_Company_FavouriteCompaniesId",
                        column: x => x.FavouriteCompaniesId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FavouriteCompanies_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FavouriteCompanies_UserId",
                table: "FavouriteCompanies",
                column: "UserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FavouriteCompanies");
        }
    }
}
