﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PromocodeAggregator.Api.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddCategorySource : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Updated",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Proposition");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Company");

            migrationBuilder.AddColumn<string>(
                name: "Source",
                table: "Category",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Source",
                table: "Category");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Updated",
                table: "User",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Updated",
                table: "Proposition",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Updated",
                table: "Company",
                type: "timestamp with time zone",
                nullable: true);
        }
    }
}
