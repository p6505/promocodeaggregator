﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PromocodeAggregator.Api.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddCompanyCategory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Proposition_PropositionType_PropositionTypeId",
                table: "Proposition");

            migrationBuilder.DropIndex(
                name: "IX_Proposition_PropositionTypeId",
                table: "Proposition");

            migrationBuilder.AddColumn<Guid>(
                name: "CategoryId",
                table: "Company",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Created = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Company_CategoryId",
                table: "Company",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Category_CategoryId",
                table: "Company",
                column: "CategoryId",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Category_CategoryId",
                table: "Company");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropIndex(
                name: "IX_Company_CategoryId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Company");

            migrationBuilder.CreateIndex(
                name: "IX_Proposition_PropositionTypeId",
                table: "Proposition",
                column: "PropositionTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Proposition_PropositionType_PropositionTypeId",
                table: "Proposition",
                column: "PropositionTypeId",
                principalTable: "PropositionType",
                principalColumn: "Id");
        }
    }
}
