﻿using Microsoft.EntityFrameworkCore;
using PromocodeAggregator.Api.Data.Entities;
using PromocodeAggregator.Api.Infrastructure.Abstractions;

namespace PromocodeAggregator.Api.Infrastructure;

public class EfRepository : IRepository
{
	private const int MaxTake = 500;

	private readonly DataContext _dataContext;

	public EfRepository(DataContext dataContext)
	{
		_dataContext = dataContext;
	}

	public Task AddCompany(Company company, CancellationToken cancellationToken) =>
		_dataContext.Companies.AddAsync(company, cancellationToken).AsTask();

	public Task<Company> FindCompanyByName(string name, CancellationToken cancellationToken) =>
		_dataContext.Companies.FirstOrDefaultAsync(x => x.Name == name, cancellationToken);

	public async Task<Category> FindOrAddCategory(string name, string description, string source)
	{
		var category = await _dataContext.Categories
			.FirstOrDefaultAsync(x => x.Name == name);

		if (category == null)
		{
			category = new Category(name, description, source);
			_dataContext.Add(category);
		}

		return category;
	}

	public async Task<Company> FindOrAddCompany(
		string requestCompanyName,
		string source,
		string imageLink,
		string imageBase64,
		Category category)
	{
		var name = requestCompanyName.Trim();
		var company = await _dataContext.Companies
			.Include(x => x.Propositions)
			.FirstOrDefaultAsync(x => x.Name == name);

		if (company == null)
		{
			company = new Company(name, source, imageLink, imageBase64, category);
			_dataContext.Add(company);
		}

		await _dataContext.SaveChangesAsync();
		return company;
	}

	public Task<Company> FindCompanyById(Guid id, CancellationToken cancellationToken) =>
		_dataContext.Companies.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

	public Task<List<Company>> GetCompanies(
		string query,
		Guid? categoryId,
		int skip,
		int take,
		CancellationToken cancellationToken)
	{
		var queryable = _dataContext.Companies.AsQueryable();

		if (!string.IsNullOrWhiteSpace(query))
		{
			var pattern = $"%{query}%";
			queryable = queryable.Where(x => EF.Functions.ILike(x.Name, pattern));
		}

		if (categoryId.HasValue)
		{
			queryable = queryable.Where(x => x.CategoryId == categoryId.Value);
		}

		return queryable
			.OrderBy(x => x.Created)
			.Skip(skip)
			.Take(Math.Min(take, MaxTake))
			.ToListAsync(cancellationToken);
	}

	public Task<List<Category>> GetCategories(string query, CancellationToken cancellationToken)
	{
		var queryable = _dataContext.Categories.AsQueryable();

		if (!string.IsNullOrWhiteSpace(query))
		{
			var pattern = $"%{query}%";
			queryable = queryable
				.Where(x =>
					EF.Functions.ILike(x.Name, pattern)
					|| EF.Functions.ILike(x.Description, pattern));
		}

		return queryable.OrderBy(x => x.Name).ToListAsync(cancellationToken: cancellationToken);
	}

	public Task<Proposition> GetPropositionById(Guid id, CancellationToken cancellationToken) =>
		_dataContext.Propositions
			.Include(x => x.Company)
			.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

	public Task<Category> GetCategoryById(Guid id, CancellationToken cancellationToken) =>
		_dataContext.Categories.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

	public Task<List<Proposition>> GetPropositionsForCompany(
		Guid companyId,
		string query,
		int skip,
		int take,
		CancellationToken cancellationToken)
	{
		var queryable = _dataContext.Propositions
			.Where(x => x.CompanyId == companyId);
		// && (x.Expiration == null || x.Expiration >= DateTimeOffset.UtcNow))

		return GetPropositions(queryable, query, skip, take, cancellationToken);
	}

	public Task<List<Proposition>> GetPropositionByQuery(
		string query,
		int skip,
		int take,
		CancellationToken cancellationToken)
	{
		var queryable = _dataContext.Propositions
			// && (x.Expiration == null || x.Expiration >= DateTimeOffset.UtcNow))
			.AsQueryable();

		return GetPropositions(queryable, query, skip, take, cancellationToken);
	}

	private Task<List<Proposition>> GetPropositions(
		IQueryable<Proposition> queryable,
		string query,
		int skip,
		int take,
		CancellationToken cancellationToken)
	{
		if (!string.IsNullOrWhiteSpace(query))
		{
			var pattern = $"%{query}%";
			queryable = queryable.Where(x => EF.Functions.ILike(x.Name, pattern)
			                                 || EF.Functions.ILike(x.Description, pattern)
			                                 || EF.Functions.ILike(x.PropositionValue, pattern));
		}

		return queryable.OrderByDescending(x => x.Created)
			.Skip(skip)
			.Take(Math.Min(take, MaxTake))
			.ToListAsync(cancellationToken);
	}

	public async Task AddOrUpdateProposition(Company company, List<Proposition> propositions)
	{
		var nonExistingPropositions = propositions
			.Where(x => company.Propositions.All(y => y.PropositionValue != x.PropositionValue))
			.ToArray();

		foreach (var nonExitingProposition in nonExistingPropositions)
		{
			_dataContext.Add(nonExitingProposition);
		}

		company.AddPropositions(nonExistingPropositions);
		await _dataContext.SaveChangesAsync();
	}

	public async Task<User> GetOrAddUser(Guid userId, CancellationToken cancellationToken)
	{
		var user = await _dataContext.Users.FirstOrDefaultAsync(x => x.Id == userId, cancellationToken);

		if (user != null)
		{
			return user;
		}

		user = new User(userId);

		await _dataContext.AddAsync(user, cancellationToken);
		return user;
	}

	public Task<User> FindUser(Guid userId, CancellationToken cancellationToken) => _dataContext.Users
		.Include(x => x.FavouriteProposition)
		.Include(x => x.FavouriteCompanies)
		.FirstOrDefaultAsync(x => x.Id == userId, cancellationToken);

	public Task<List<Company>> GetMostPopularCompanies(int skip, int limit, CancellationToken cancellationToken) =>
		_dataContext.Companies
			.Where(x => x.SearchCount > 0)
			.OrderByDescending(x => x.SearchCount)
			.Skip(skip)
			.Take(Math.Min(limit, MaxTake))
			.ToListAsync(cancellationToken: cancellationToken);

	public Task<List<Proposition>> GetMostPopularPropositions(
		int skip,
		int limit,
		CancellationToken cancellationToken) =>
		_dataContext.Propositions
			.Where(x => x.SearchCount > 0)
			.OrderByDescending(x => x.SearchCount)
			.Skip(skip)
			.Take(Math.Min(limit, MaxTake))
			.ToListAsync(cancellationToken: cancellationToken);

	public async Task SaveChanges(CancellationToken cancellationToken) =>
		await _dataContext.SaveChangesAsync(cancellationToken);
}
