﻿using FluentValidation;

namespace PromocodeAggregator.Api.Domain.Requests.Companies;

public class AddCompanyRequestValidator : AbstractValidator<AddCompanyRequest>
{
	public AddCompanyRequestValidator()
	{
		RuleFor(x => x.Name)
			.NotEmpty();
	}
}
