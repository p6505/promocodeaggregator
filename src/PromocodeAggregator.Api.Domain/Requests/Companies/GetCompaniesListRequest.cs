﻿using AutoMapper;
using Mediator;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Client.Models;
using PromocodeAggregator.Client.Models.Companies;

namespace PromocodeAggregator.Api.Domain.Requests.Companies;

public class GetCompaniesListRequest : GetCompaniesListRequestModel, IRequest<CollectionModel<CompanyModel>>
{
}

public class GetCompaniesListRequestHandler : IRequestHandler<GetCompaniesListRequest, CollectionModel<CompanyModel>>
{
	private readonly IRepository _repository;
	private readonly IMapper _mapper;

	public GetCompaniesListRequestHandler(IRepository repository, IMapper mapper)
	{
		_repository = repository;
		_mapper = mapper;
	}

	public async ValueTask<CollectionModel<CompanyModel>> Handle(
		GetCompaniesListRequest request,
		CancellationToken cancellationToken)
	{
		var entities = await _repository.GetCompanies(
			request.Query,
			request.CategoryId,
			request.Skip,
			request.Take,
			cancellationToken);

		var items = _mapper.Map<CompanyModel[]>(entities);

		return new CollectionModel<CompanyModel>(items);
	}
}
