﻿namespace PromocodeAggregator.Api.Domain.Requests.Companies;

public record struct DuplicateCompany;
