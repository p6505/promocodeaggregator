﻿using AutoMapper;
using Mediator;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Client.Models;
using PromocodeAggregator.Client.Models.Companies;

namespace PromocodeAggregator.Api.Domain.Requests.Companies;

public class GetMostPopularCompaniesRequest : PaginationModel, IRequest<CollectionModel<CompanyStatisticModel>>
{
}

public class GetMostPopularCompaniesRequestHandler :
	IRequestHandler<GetMostPopularCompaniesRequest, CollectionModel<CompanyStatisticModel>>
{
	private readonly IRepository _repository;
	private readonly IMapper _mapper;

	public GetMostPopularCompaniesRequestHandler(IRepository repository, IMapper mapper)
	{
		_repository = repository;
		_mapper = mapper;
	}

	public async ValueTask<CollectionModel<CompanyStatisticModel>> Handle(
		GetMostPopularCompaniesRequest request,
		CancellationToken cancellationToken)
	{
		var entities = await _repository.GetMostPopularCompanies(request.Skip, request.Take, cancellationToken);

		var items = _mapper.Map<List<CompanyStatisticModel>>(entities);

		return new CollectionModel<CompanyStatisticModel>(items);
	}
}
