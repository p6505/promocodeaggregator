﻿using AutoMapper;
using Mediator;
using PromocodeAggregator.Api.Data.Entities;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Client.Models.Companies;

namespace PromocodeAggregator.Api.Domain.Requests.Companies;

public class AddCompanyRequest : AddCompanyRequestModel, IRequest<OneOf<CompanyModel, DuplicateCompany>>
{
}

public class AddCompanyRequestHandler : IRequestHandler<AddCompanyRequest, OneOf<CompanyModel, DuplicateCompany>>
{
	private readonly IRepository _repository;
	private readonly IMapper _mapper;

	public AddCompanyRequestHandler(IRepository repository, IMapper mapper)
	{
		_repository = repository;
		_mapper = mapper;
	}

	public async ValueTask<OneOf<CompanyModel, DuplicateCompany>> Handle(
		AddCompanyRequest request,
		CancellationToken cancellationToken)
	{
		if (await _repository.FindCompanyByName(request.Name, cancellationToken) != null)
		{
			return new DuplicateCompany();
		}

		var propositions = _mapper.Map<List<Proposition>>(request.Propositions);
		var category = await _repository.FindOrAddCategory(
			request.Category.Name,
			request.Category.Description,
			request.Category.Source);
		var company = new Company(request.Name,
			request.Source,
			request.ImageBase64,
			request.ImageLink,
			category);

		company.AddPropositions(propositions);
		await _repository.SaveChanges(cancellationToken);

		return _mapper.Map<CompanyModel>(company);
	}
}
