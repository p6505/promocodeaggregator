﻿using AutoMapper;
using Mediator;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Client.Models;
using PromocodeAggregator.Client.Models.Propositions;

namespace PromocodeAggregator.Api.Domain.Requests.Propositions;

public class GetPropositionListRequest : GetPropositionListRequestModel, IRequest<CollectionModel<PropositionModel>>
{
}

public class GetPromoCodesListRequestHandler :
	IRequestHandler<GetPropositionListRequest, CollectionModel<PropositionModel>>
{
	private readonly IRepository _repository;
	private readonly IMapper _mapper;

	public GetPromoCodesListRequestHandler(IRepository repository, IMapper mapper)
	{
		_repository = repository;
		_mapper = mapper;
	}

	public async ValueTask<CollectionModel<PropositionModel>> Handle(
		GetPropositionListRequest request,
		CancellationToken cancellationToken)
	{
		if (request.CompanyId is { } companyId)
		{
			var company = await _repository.FindCompanyById(companyId, cancellationToken);

			if (company != null)
			{
				company.IncSearchesCount();
				await _repository.SaveChanges(cancellationToken);
			}
			else
			{
				return new CollectionModel<PropositionModel>();
			}

			var entities = await _repository.GetPropositionsForCompany(
				companyId,
				request.Query,
				request.Skip,
				request.Take,
				cancellationToken);

			var items = _mapper.Map<PropositionModel[]>(entities);

			return new CollectionModel<PropositionModel>(items);
		}
		else
		{
			var entities = await _repository.GetPropositionByQuery(
				request.Query,
				request.Skip,
				request.Take,
				cancellationToken);

			var items = _mapper.Map<PropositionModel[]>(entities);

			return new CollectionModel<PropositionModel>(items);
		}
	}
}
