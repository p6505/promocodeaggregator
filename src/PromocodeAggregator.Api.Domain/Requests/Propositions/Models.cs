﻿namespace PromocodeAggregator.Api.Domain.Requests.Propositions;

public record struct CompanyNotFound;

public record struct DuplicateProposition;
