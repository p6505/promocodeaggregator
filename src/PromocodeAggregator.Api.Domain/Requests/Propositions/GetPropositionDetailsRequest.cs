﻿using AutoMapper;
using Mediator;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Client.Models.Propositions;

namespace PromocodeAggregator.Api.Domain.Requests.Propositions;

public record GetPropositionDetailsRequest(Guid Id) : IRequest<OneOf<PropositionNotFound, PropositionDetailsModel>>;

public class GetPropositionDetailsRequestHandler :
	IRequestHandler<GetPropositionDetailsRequest, OneOf<PropositionNotFound, PropositionDetailsModel>>
{
	private readonly IRepository _repository;
	private readonly IMapper _mapper;

	public GetPropositionDetailsRequestHandler(IRepository repository, IMapper mapper)
	{
		_repository = repository;
		_mapper = mapper;
	}

	public async ValueTask<OneOf<PropositionNotFound, PropositionDetailsModel>> Handle(
		GetPropositionDetailsRequest request,
		CancellationToken cancellationToken)
	{
		var entity = await _repository.GetPropositionById(request.Id, cancellationToken);

		if (entity == null)
		{
			return new PropositionNotFound();
		}

		entity.IncSearchesCount();
		await _repository.SaveChanges(cancellationToken);

		return _mapper.Map<PropositionDetailsModel>(entity);
	}
}
