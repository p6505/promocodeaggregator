﻿using AutoMapper;
using Mediator;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Client.Models;
using PromocodeAggregator.Client.Models.Propositions;

namespace PromocodeAggregator.Api.Domain.Requests.Propositions;

public class GetMostPopularPropositionsRequest : PaginationModel, IRequest<CollectionModel<PropositionStatisticModel>>
{
}

public record GetMostPopularPropositionsRequestHandler :
	IRequestHandler<GetMostPopularPropositionsRequest, CollectionModel<PropositionStatisticModel>>
{
	private readonly IRepository _repository;
	private readonly IMapper _mapper;

	public GetMostPopularPropositionsRequestHandler(IRepository repository, IMapper mapper)
	{
		_repository = repository;
		_mapper = mapper;
	}

	public async ValueTask<CollectionModel<PropositionStatisticModel>> Handle(
		GetMostPopularPropositionsRequest request,
		CancellationToken cancellationToken)
	{
		var entities = await _repository.GetMostPopularPropositions(request.Skip, request.Take, cancellationToken);

		var items = _mapper.Map<List<PropositionStatisticModel>>(entities);

		return new CollectionModel<PropositionStatisticModel>(items);
	}
}
