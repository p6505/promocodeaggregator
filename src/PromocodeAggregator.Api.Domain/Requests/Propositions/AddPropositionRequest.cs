﻿using Mediator;
using PromocodeAggregator.Api.Data.Entities;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Client.Models.Propositions;

namespace PromocodeAggregator.Api.Domain.Requests.Propositions;

public class AddPropositionRequest : AddPropositionRequestModel, IRequest<OneOf<Success, CompanyNotFound>>
{
}

public class AddPromoCodeRequestHandler : IRequestHandler<AddPropositionRequest, OneOf<Success, CompanyNotFound>>
{
	private readonly IRepository _repository;

	public AddPromoCodeRequestHandler(IRepository repository)
	{
		_repository = repository;
	}

	public async ValueTask<OneOf<Success, CompanyNotFound>> Handle(
		AddPropositionRequest request,
		CancellationToken cancellationToken)
	{
		var company = await _repository.FindCompanyById(request.CompanyId, cancellationToken);

		if (company == null)
		{
			return new CompanyNotFound();
		}

		var propositions = new List<Proposition>
		{
			new(
				PropositionType.Promocode,
				request.Value,
				request.Expiration,
				request.Name,
				request.Description,
				request.SourceUrl)
		};
		company.AddPropositions(propositions);
		await _repository.SaveChanges(cancellationToken);

		return new Success();
	}
}
