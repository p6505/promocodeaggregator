﻿using Mediator;
using Microsoft.Extensions.Logging;
using PromocodeAggregator.Api.Data.Entities;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Broker.Messages;

namespace PromocodeAggregator.Api.Domain.Requests.Propositions;

public class SavePollingResultRequest : PollingResultMessageModel, IRequest
{
}

public class SavePollingResultRequestHandler : IRequestHandler<SavePollingResultRequest>
{
	private readonly IRepository _repository;
	private readonly ILogger<SavePollingResultRequestHandler> _logger;

	public SavePollingResultRequestHandler(IRepository repository, ILogger<SavePollingResultRequestHandler> logger)
	{
		_repository = repository;
		_logger = logger;
	}

	public async ValueTask<Unit> Handle(SavePollingResultRequest request, CancellationToken cancellationToken)
	{
		var requestCompany = request.Company;

		if (string.IsNullOrWhiteSpace(requestCompany.Name))
		{
			_logger.LogInformation(
				$"Company name is empty. Url: {requestCompany.Source}. Codes saving will be skipped");
			return Unit.Value;
		}

		var category = await _repository.FindOrAddCategory(
			requestCompany.Category.Name,
			requestCompany.Category.Description,
			requestCompany.Category.Source);

		var company = await _repository.FindOrAddCompany(
			requestCompany.Name,
			requestCompany.Source,
			requestCompany.ImageLink,
			requestCompany.ImageBase64,
			category);

		var propositions = ConvertPropositions(request.Propositions);

		await _repository.AddOrUpdateProposition(company, propositions);

		return Unit.Value;
	}

	private static List<Proposition> ConvertPropositions(IEnumerable<PropositionItemMessageModel> codes) =>
		codes.Select(x => new Proposition(
				x.PropositionKind is PropositionKind.Promocode
					? PropositionType.Promocode
					: PropositionType.Promotion,
				x.Value,
				x.Expiration?.ToUniversalTime(),
				x.Name,
				x.Description,
				x.Link))
			.ToList();
}
