﻿namespace PromocodeAggregator.Api.Domain.Requests;

public record struct UserNotFound;

public record struct PropositionNotFound;
