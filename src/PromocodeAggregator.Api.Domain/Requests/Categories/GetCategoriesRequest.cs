﻿using AutoMapper;
using Mediator;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Client.Models;

namespace PromocodeAggregator.Api.Domain.Requests.Categories;

public record GetCategoriesRequest(string Query) : IRequest<CollectionModel<CategoryModel>>;

public record GetCategoryByIdRequest(Guid Id) : IRequest<CategoryModel>;

public class CategoriesRequestHandler :
	IRequestHandler<GetCategoriesRequest, CollectionModel<CategoryModel>>,
	IRequestHandler<GetCategoryByIdRequest, CategoryModel>
{
	private readonly IRepository _repository;
	private readonly IMapper _mapper;

	public CategoriesRequestHandler(IRepository repository, IMapper mapper)
	{
		_repository = repository;
		_mapper = mapper;
	}

	public async ValueTask<CollectionModel<CategoryModel>> Handle(
		GetCategoriesRequest request,
		CancellationToken cancellationToken)
	{
		var categories = await _repository.GetCategories(request.Query, cancellationToken);

		var items = _mapper.Map<List<CategoryModel>>(categories);

		return new CollectionModel<CategoryModel>(items);
	}

	public async ValueTask<CategoryModel> Handle(GetCategoryByIdRequest byIdRequest, CancellationToken cancellationToken)
	{
		var category = await _repository.GetCategoryById(byIdRequest.Id, cancellationToken);

		var result = _mapper.Map<CategoryModel>(category);

		return result;
	}
}
