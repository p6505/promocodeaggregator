﻿using Mediator;
using PromocodeAggregator.Api.Domain.Requests.Propositions;
using PromocodeAggregator.Api.Infrastructure.Abstractions;

namespace PromocodeAggregator.Api.Domain.Requests.Users;

public record AddFavouriteCompanyRequest
	(Guid CompanyId, Guid UserId) : IRequest<OneOf<Success, CompanyNotFound>>;

public record RemoveFavouriteCompanyRequest
	(Guid CompanyId, Guid UserId) : IRequest<OneOf<Success, UserNotFound, CompanyNotFound>>;

public record ProcessFavouriteCompanyRequestHandler :
	IRequestHandler<AddFavouriteCompanyRequest, OneOf<Success, CompanyNotFound>>,
	IRequestHandler<RemoveFavouriteCompanyRequest, OneOf<Success, UserNotFound, CompanyNotFound>>
{
	private readonly IRepository _repository;

	public ProcessFavouriteCompanyRequestHandler(IRepository repository)
	{
		_repository = repository;
	}


	public async ValueTask<OneOf<Success, CompanyNotFound>> Handle(
		AddFavouriteCompanyRequest request,
		CancellationToken cancellationToken)
	{
		var company = await _repository.FindCompanyById(request.CompanyId, cancellationToken);

		if (company == null)
		{
			return new CompanyNotFound();
		}

		var user = await _repository.GetOrAddUser(request.UserId, cancellationToken);

		user.AddFavouriteCompany(company);

		await _repository.SaveChanges(cancellationToken);

		return new Success();
	}

	public async ValueTask<OneOf<Success, UserNotFound, CompanyNotFound>> Handle(
		RemoveFavouriteCompanyRequest request,
		CancellationToken cancellationToken)
	{
		var company = await _repository.FindCompanyById(request.CompanyId, cancellationToken);

		if (company == null)
		{
			return new CompanyNotFound();
		}

		var user = await _repository.FindUser(request.UserId, cancellationToken);

		if (user == null)
		{
			return new UserNotFound();
		}

		user.RemoveFavouriteCompany(company);

		await _repository.SaveChanges(cancellationToken);

		return new Success();
	}
}
