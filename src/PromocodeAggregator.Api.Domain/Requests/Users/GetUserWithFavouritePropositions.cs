﻿using AutoMapper;
using Mediator;
using PromocodeAggregator.Api.Domain.Requests.Propositions;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Client.Models;
using PromocodeAggregator.Client.Models.Companies;
using PromocodeAggregator.Client.Models.Propositions;

namespace PromocodeAggregator.Api.Domain.Requests.Users;

public record GetUserFavouritePropositionsRequest(Guid UserId) :
	IRequest<OneOf<UserNotFound, CollectionModel<PropositionModel>>>;

public record GetUserFavouriteCompaniesRequest(Guid UserId) :
	IRequest<OneOf<UserNotFound, CollectionModel<CompanyModel>>>;

public class GetUserFavouritePropositionsRequestHandler :
	IRequestHandler<GetUserFavouritePropositionsRequest, OneOf<UserNotFound, CollectionModel<PropositionModel>>>,
	IRequestHandler<GetUserFavouriteCompaniesRequest, OneOf<UserNotFound, CollectionModel<CompanyModel>>>
{
	private readonly IRepository _repository;
	private readonly IMapper _mapper;

	public GetUserFavouritePropositionsRequestHandler(IRepository repository, IMapper mapper)
	{
		_repository = repository;
		_mapper = mapper;
	}

	public async ValueTask<OneOf<UserNotFound, CollectionModel<PropositionModel>>> Handle(
		GetUserFavouritePropositionsRequest request,
		CancellationToken cancellationToken)
	{
		var user = await _repository.FindUser(request.UserId, cancellationToken);

		if (user == null)
		{
			return new UserNotFound();
		}

		var items = _mapper.Map<List<PropositionModel>>(user.FavouriteProposition);
		return new CollectionModel<PropositionModel>(items);
	}

	public async ValueTask<OneOf<UserNotFound, CollectionModel<CompanyModel>>> Handle(
		GetUserFavouriteCompaniesRequest request,
		CancellationToken cancellationToken)
	{
		var user = await _repository.FindUser(request.UserId, cancellationToken);

		if (user == null)
		{
			return new UserNotFound();
		}

		var items = _mapper.Map<List<CompanyModel>>(user.FavouriteCompanies);
		return new CollectionModel<CompanyModel>(items);
	}
}
