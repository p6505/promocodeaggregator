﻿using Mediator;
using PromocodeAggregator.Api.Infrastructure.Abstractions;

namespace PromocodeAggregator.Api.Domain.Requests.Users;

public record AddFavouritePropositionRequest
	(Guid PropositionId, Guid UserId) : IRequest<OneOf<Success, PropositionNotFound>>;

public record RemoveFavouritePropositionRequest
	(Guid PropositionId, Guid UserId) : IRequest<OneOf<Success, UserNotFound, PropositionNotFound>>;

public record ProcessFavouritePropositionRequestHandler :
	IRequestHandler<AddFavouritePropositionRequest, OneOf<Success, PropositionNotFound>>,
	IRequestHandler<RemoveFavouritePropositionRequest, OneOf<Success, UserNotFound, PropositionNotFound>>
{
	private readonly IRepository _repository;

	public ProcessFavouritePropositionRequestHandler(IRepository repository)
	{
		_repository = repository;
	}


	public async ValueTask<OneOf<Success, PropositionNotFound>> Handle(
		AddFavouritePropositionRequest request,
		CancellationToken cancellationToken)
	{
		var proposition = await _repository.GetPropositionById(request.PropositionId, cancellationToken);

		if (proposition == null)
		{
			return new PropositionNotFound();
		}

		var user = await _repository.GetOrAddUser(request.UserId, cancellationToken);

		user.AddFavouriteProposition(proposition);

		await _repository.SaveChanges(cancellationToken);

		return new Success();
	}

	public async ValueTask<OneOf<Success, UserNotFound, PropositionNotFound>> Handle(
		RemoveFavouritePropositionRequest request,
		CancellationToken cancellationToken)
	{
		var proposition = await _repository.GetPropositionById(request.PropositionId, cancellationToken);

		if (proposition == null)
		{
			return new PropositionNotFound();
		}

		var user = await _repository.FindUser(request.UserId, cancellationToken);

		if (user == null)
		{
			return new UserNotFound();
		}

		user.RemoveFavouriteProposition(proposition);

		await _repository.SaveChanges(cancellationToken);

		return new Success();
	}
}
