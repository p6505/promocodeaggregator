﻿using FluentValidation;
using PromocodeAggregator.Api.Domain.Requests.Propositions;

namespace PromocodeAggregator.Api.Domain.Validators;

public class AddPromoCodeRequestValidator : AbstractValidator<AddPropositionRequest>
{
	private const int MinimumDescriptionLength = 8;

	public AddPromoCodeRequestValidator()
	{
		RuleFor(x => x.Value)
			.NotEmpty();

		RuleFor(x => x.Expiration)
			.Must(date => date == null || date > DateTimeOffset.Now);

		RuleFor(x => x.Name)
			.MinimumLength(MinimumDescriptionLength)
			.NotEmpty();

		RuleFor(x => x.Description)
			.MinimumLength(MinimumDescriptionLength)
			.NotEmpty();

		RuleFor(x => x.SourceUrl)
			.Must(url => Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
			.WithMessage("'Source Url' is not url!");
	}
}
