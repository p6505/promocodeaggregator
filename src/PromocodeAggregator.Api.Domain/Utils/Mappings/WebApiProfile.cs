﻿using AutoMapper;
using PromocodeAggregator.Api.Data.Entities;
using PromocodeAggregator.Client.Models;
using PromocodeAggregator.Client.Models.Companies;
using PromocodeAggregator.Client.Models.Propositions;

namespace PromocodeAggregator.Api.Domain.Utils.Mappings;

public class WebApiProfile : Profile
{
	public WebApiProfile()
	{
		CreateMap<Company, CompanyModel>();
		CreateMap<Company, CompanyStatisticModel>();

		CreateMap<Proposition, PropositionModel>();
		CreateMap<Proposition, PropositionDetailsModel>()
			.ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.PropositionValue));
		CreateMap<Proposition, PropositionStatisticModel>();

		CreateMap<Category, CategoryModel>();
	}
}
