﻿using Microsoft.Extensions.DependencyInjection;

namespace PromocodeAggregator.Api.Domain.Extensions;

public static class ServiceCollectionExtensions
{
	public static IServiceCollection AddDomain(this IServiceCollection services) =>
		services.AddMediator(options => options.ServiceLifetime = ServiceLifetime.Scoped);
}
