using System.Diagnostics;

namespace PromocodeAggregator.Broker.Messages;

[DebuggerDisplay("Code: {Value}. Link: {Link}. Name: {Name}")]
public class PropositionItemMessageModel
{
	public string Name { get; set; }

	public string Value { get; set; }

	public DateTimeOffset? Expiration { get; set; }

	public PropositionKind PropositionKind { get; set; }

	public string Link { get; set; }

	public string Description { get; set; }
}
