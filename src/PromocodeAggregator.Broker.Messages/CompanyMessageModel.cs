using System.Diagnostics;

namespace PromocodeAggregator.Broker.Messages;

[DebuggerDisplay("{Name} {Source}")]
public class CompanyMessageModel
{
	public string Name { get; set; }

	public CategoryMessageModel Category { get; set; }

	public string Source { get; set; }

	public string ImageLink { get; set; }

	public string ImageBase64 { get; set; }
}
