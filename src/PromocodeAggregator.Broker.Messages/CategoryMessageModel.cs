using System.Diagnostics;

namespace PromocodeAggregator.Broker.Messages;

[DebuggerDisplay("{Name}")]
public class CategoryMessageModel
{
	public string Name { get; set; }

	public string Description { get; set; }

	public string Source { get; set; }
}
