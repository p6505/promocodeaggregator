// ReSharper disable UnusedMember.Global

namespace PromocodeAggregator.Broker.Messages;

public enum PropositionKind
{
	Promocode,
	Promotion
}
