namespace PromocodeAggregator.Broker.Messages;

public class PollingResultMessageModel
{
	public int SourceId { get; set; }

	public CompanyMessageModel Company { get; set; }

	public List<PropositionItemMessageModel> Propositions { get; set; }
}
