﻿using System.Security.Claims;
using System.Text.Json;

namespace PromocodeAggregator.Api.Extension;

public static class ClaimsPrincipalExtensions
{
	public static Guid? GetId(this ClaimsPrincipal claimsPrincipal)
	{
		var stringValue = claimsPrincipal.FindFirstValue(ClaimTypes.NameIdentifier);

		return Guid.TryParse(stringValue, out var id)
			? id
			: null;
	}

	private const string RolesPropertyName = "realm_access";

	public static bool HasRealmRole(this ClaimsPrincipal claimsPrincipal, string roleName)
	{
		var claimValue = claimsPrincipal.FindFirst(RolesPropertyName)?.Value;

		if (claimValue == null)
		{
			return false;
		}

		try
		{
			var document = JsonDocument.Parse(claimValue);

			return document.RootElement.GetProperty("roles").EnumerateArray().Any(x => x.GetString() == roleName);
		}
		catch (Exception)
		{
			return false;
		}
	}
}
