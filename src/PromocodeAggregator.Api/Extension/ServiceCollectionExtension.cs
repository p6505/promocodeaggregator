﻿using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using PromocodeAggregator.Api.Options;
using RestEase;

namespace PromocodeAggregator.Api.Extension;

public static class ServiceCollectionExtension
{
	public static IServiceCollection AddProblemDetailsExtension(this IServiceCollection services)
	{
		services.AddProblemDetails(options =>
		{
			options.Map<ApiException>(exception => new ProblemDetails
			{
				Status = (int)exception.StatusCode,
				Title = exception.Content,
				Detail = exception.Message
			});

			options.Map<Exception>(e => new ProblemDetails
			{
				Status = StatusCodes.Status500InternalServerError,
				Type = e.GetType().Name,
				Detail = e.StackTrace,
				Title = e.Message
			});
		});

		return services;
	}

	public static IServiceCollection AddSwaggerSecurity(
		this IServiceCollection services,
		KeycloakOptions keycloakOptions)
	{
		services.ConfigureSwaggerGen(options =>
		{
			options.AddSecurityDefinition("Bearer",
				new OpenApiSecurityScheme
				{
					In = ParameterLocation.Header,
					Type = SecuritySchemeType.Http,
					Description = "Bearer token authorization",
					Scheme = "Bearer",
					Name = "Authorization"
				});

			options.AddSecurityDefinition("oauth2",
				new OpenApiSecurityScheme
				{
					Type = SecuritySchemeType.OAuth2,
					Description = "oauth2 authorization",
					Scheme = "oauth2",
					Flows = new OpenApiOAuthFlows
					{
						Password = new OpenApiOAuthFlow
						{
							AuthorizationUrl = new Uri(keycloakOptions.BaseAddress + "/protocol/openid-connect/auth"),
							TokenUrl = new Uri(keycloakOptions.BaseAddress + "/protocol/openid-connect/token")
						}
					}
				});

			options.AddSecurityRequirement(new OpenApiSecurityRequirement
			{
				{
					new OpenApiSecurityScheme
					{
						Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
					},
					new List<string>()
				},
				{
					new OpenApiSecurityScheme
					{
						Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
					},
					new List<string>()
				}
			});
		});

		return services;
	}
}
