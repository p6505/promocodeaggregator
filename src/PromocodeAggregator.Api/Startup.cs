using FluentValidation;
using FluentValidation.AspNetCore;
using Hellang.Middleware.ProblemDetails;
using Microservices.Kafka.BackgroundServices.Mediator.Extensions;
using Microservices.Kafka.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Prometheus.Client.HttpRequestDurations;
using PromocodeAggregator.Api.Auth;
using PromocodeAggregator.Api.Domain.Extensions;
using PromocodeAggregator.Api.Domain.Requests.Propositions;
using PromocodeAggregator.Api.Extension;
using PromocodeAggregator.Api.Infrastructure;
using PromocodeAggregator.Api.Infrastructure.Abstractions;
using PromocodeAggregator.Api.Options;

namespace PromocodeAggregator.Api;

public class Startup
{
	private readonly IConfiguration _configuration;

	public Startup(IConfiguration configuration)
	{
		_configuration = configuration;
	}

	public void ConfigureServices(IServiceCollection services)
	{
		var connectionString = _configuration.GetConnectionString(nameof(DataContext));
		var asm = AppDomain.CurrentDomain.GetAssemblies().Where(x => !x.IsDynamic).ToArray();
		var keycloakOptions = _configuration.GetRequiredSection(nameof(KeycloakOptions)).Get<KeycloakOptions>();
		ValidatorOptions.Global.LanguageManager.Enabled = false;

		services
			.AddProblemDetailsExtension()
			.AddSwaggerSecurity(keycloakOptions)
			.AddFluentValidationAutoValidation()
			.AddValidatorsFromAssembly(typeof(Startup).Assembly)
			.AddDomain()
			.AddAutoMapper(asm);

		services
			.AddDbContext<DataContext>(options => options.UseNpgsql(connectionString))
			.AddScoped<IRepository, EfRepository>();

		services
			.AddKafkaMessageRequestConsumer<SavePollingResultRequest>(
				_configuration,
				"HabrPromocodesConsumer",
				"KafkaOptions")
			.AddKafkaSystemTextJsonSerializers()
			.AddKafkaProducer(_configuration, "KafkaOptions");

		services
			.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
			.AddJwtBearer(JwtBearerDefaults.AuthenticationScheme,
				options =>
				{
					options.Authority = keycloakOptions.BaseAddress;
					options.RequireHttpsMetadata = false;
					options.TokenValidationParameters.ValidateAudience = false;
				});

		services.AddAuthorization(options => options.AddPolicy(nameof(Policies.RequireModerator),
			builder => builder.Requirements.Add(
				new AssertionRequirement(context => context.User.HasRealmRole("moderator")))));

		services.AddControllers();
	}

	public void Configure(IApplicationBuilder app)
	{
		app.EnsureMigrationOfContext<DataContext>();

		app.UseProblemDetails();
		app.UsePrometheusRequestDurations(options =>
		{
			options.IncludeMethod = true;
			options.IncludePath = true;
			options.IncludeStatusCode = true;
		});

		app.UseRouting();

		app.UseAuthentication();
		app.UseAuthorization();

		app.UseEndpoints(endpoints => endpoints.MapControllers());
	}
}
