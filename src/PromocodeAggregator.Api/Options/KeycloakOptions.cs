﻿namespace PromocodeAggregator.Api.Options;

public class KeycloakOptions
{
	public string HostAddress { get; set; }

	public string Realm { get; set; }
	
	public string BaseAddress => $"{HostAddress}/auth/realms/{Realm}";
}
