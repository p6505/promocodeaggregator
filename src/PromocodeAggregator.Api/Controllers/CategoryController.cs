﻿using Mediator;
using Microsoft.AspNetCore.Mvc;
using PromocodeAggregator.Api.Domain.Requests.Categories;
using PromocodeAggregator.Client.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace PromocodeAggregator.Api.Controllers;

[ApiController]
[Route("api/categories")]
[SwaggerTag("Действия с категориями")]
public class CategoryController : ControllerBase
{
	private readonly IMediator _mediator;

	public CategoryController(IMediator mediator)
	{
		_mediator = mediator;
	}

	/// <summary>
	/// Получение списка доступных категорий
	/// </summary>
	[HttpGet]
	[ProducesResponseType(typeof(CollectionModel<CategoryModel>), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetCategories([FromQuery] GetCategoriesRequest request)
	{
		var result = await _mediator.Send(request);

		return Ok(result);
	}

	/// <summary>
	/// Получение категории по id
	/// </summary>
	[HttpGet("{id:guid}")]
	[ProducesResponseType(typeof(CategoryModel), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetCategories(Guid id)
	{
		var result = await _mediator.Send(new GetCategoryByIdRequest(id));

		return Ok(result);
	}
}
