﻿using Mediator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PromocodeAggregator.Api.Auth;
using PromocodeAggregator.Api.Domain.Requests.Companies;
using PromocodeAggregator.Api.Domain.Requests.Users;
using PromocodeAggregator.Api.Extension;
using PromocodeAggregator.Client.Models;
using PromocodeAggregator.Client.Models.Companies;
using Swashbuckle.AspNetCore.Annotations;

// ReSharper disable UnusedParameter.Local

namespace PromocodeAggregator.Api.Controllers;

[ApiController]
[Route("api/companies")]
[SwaggerTag("Действия с компаниями")]
public class CompanyController : ControllerBase
{
	private readonly IMediator _mediator;

	public CompanyController(IMediator mediator)
	{
		_mediator = mediator;
	}

	/// <summary>
	/// Получение списка компаний
	/// </summary>
	[HttpGet]
	[ProducesResponseType(typeof(CollectionModel<CompanyModel>), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetList([FromQuery] GetCompaniesListRequest request)
	{
		var result = await _mediator.Send(request);

		return Ok(result);
	}

	/// <summary>
	/// Добавление компании
	/// </summary>
	[HttpPost]
	[Authorize(nameof(Policies.RequireModerator))]
	[ProducesResponseType(typeof(CompanyModel), StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	public async Task<IActionResult> Add([FromBody] AddCompanyRequest request)
	{
		var result = await _mediator.Send(request);

		return result.Match<IActionResult>(
			Ok,
			duplicateCompany => BadRequest("Duplicate company"));
	}

	/// <summary>
	/// Добавление компании в избранное
	/// </summary>
	[Authorize]
	[HttpPost("{companyId:guid}/favourite")]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	public async Task<IActionResult> AddToFavourite(Guid companyId)
	{
		var id = User.GetId();

		if (id is not { } userId)
		{
			return Unauthorized("Incorrect user");
		}

		var result = await _mediator.Send(new AddFavouriteCompanyRequest(companyId, userId));
		return result.Match<IActionResult>(
			success => Ok(),
			companyNotFound => BadRequest("Company wasn't found"));
	}

	/// <summary>
	/// Удаление компании из избранного
	/// </summary>
	[Authorize]
	[HttpDelete("{companyId:guid}/favourite")]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	public async Task<IActionResult> RemoveFromFavourite(Guid companyId)
	{
		var id = User.GetId();

		if (id is not { } userId)
		{
			return Unauthorized("Incorrect user");
		}

		var result = await _mediator.Send(new RemoveFavouriteCompanyRequest(companyId, userId));
		return result.Match<IActionResult>(
			success => Ok(),
			userNotFound => Unauthorized("User wasn't found"),
			companyNotFound => BadRequest("Company wasn't found"));
	}

	/// <summary>
	/// Получение избранных компаний для текущего пользователя
	/// </summary>
	[Authorize]
	[HttpGet("favourite")]
	[ProducesResponseType(typeof(CollectionModel<CompanyModel>), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetFavouriteList()
	{
		var id = User.GetId();

		if (id is not { } userId)
		{
			return Unauthorized("Incorrect user");
		}

		var result = await _mediator.Send(new GetUserFavouriteCompaniesRequest(userId));

		return result.Match<IActionResult>(
			userNotfound => Ok(new CollectionModel<CompanyModel>(Array.Empty<CompanyModel>())),
			Ok);
	}

	/// <summary>
	/// Получение самых популярных компаний
	/// </summary>
	[HttpGet("most-popular")]
	[ProducesResponseType(typeof(CollectionModel<CompanyStatisticModel>), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetMostPopularCompanies([FromQuery] GetMostPopularCompaniesRequest request)
	{
		var result = await _mediator.Send(request);

		return Ok(result);
	}
}
