﻿using Mediator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PromocodeAggregator.Api.Auth;
using PromocodeAggregator.Api.Domain.Requests.Propositions;
using PromocodeAggregator.Api.Domain.Requests.Users;
using PromocodeAggregator.Api.Extension;
using PromocodeAggregator.Client.Models;
using PromocodeAggregator.Client.Models.Propositions;
using Swashbuckle.AspNetCore.Annotations;

// ReSharper disable UnusedParameter.Local

namespace PromocodeAggregator.Api.Controllers;

[ApiController]
[Route("api/propositions")]
[SwaggerTag("Действия с предложениями")]
public class PropositionController : ControllerBase
{
	private readonly IMediator _mediator;

	public PropositionController(IMediator mediator)
	{
		_mediator = mediator;
	}

	/// <summary>
	/// Получение списка предложений
	/// </summary>
	[HttpGet]
	[ProducesResponseType(typeof(CollectionModel<PropositionModel>), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetList([FromQuery] GetPropositionListRequest request)
	{
		var result = await _mediator.Send(request);
		return Ok(result);
	}

	/// <summary>
	/// Получение детальной информации по предложению
	/// </summary>
	[HttpGet("{id:guid}")]
	[ProducesResponseType(typeof(PropositionDetailsModel), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetDetails(Guid id)
	{
		var result = await _mediator.Send(new GetPropositionDetailsRequest(id));
		return result.Match<IActionResult>(
			found => BadRequest("Proposition wasn't found"),
			Ok);
	}

	/// <summary>
	/// Добавление предложения
	/// </summary>
	[HttpPost]
	[Authorize(nameof(Policies.RequireModerator))]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	public async Task<IActionResult> Add([FromBody] AddPropositionRequest request)
	{
		var result = await _mediator.Send(request);
		return result.Match<IActionResult>(
			success => Ok(),
			companyNotfound => BadRequest());
	}

	/// <summary>
	/// Добавление предложения в избранное
	/// </summary>
	[Authorize]
	[HttpPost("{propositionId:guid}/favourite")]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	public async Task<IActionResult> AddToFavourite(Guid propositionId)
	{
		var id = User.GetId();

		if (id is not { } userId)
		{
			return Unauthorized("Incorrect user");
		}

		var result = await _mediator.Send(new AddFavouritePropositionRequest(propositionId, userId));
		return result.Match<IActionResult>(
			success => Ok(),
			propositionNotFound => BadRequest("Proposition wasn't found"));
	}

	/// <summary>
	/// Удаление предложения их избранного
	/// </summary>
	[Authorize]
	[HttpDelete("{propositionId:guid}/favourite")]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status400BadRequest)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	public async Task<IActionResult> RemoveFromFavourite(Guid propositionId)
	{
		var id = User.GetId();

		if (id is not { } userId)
		{
			return Unauthorized("Incorrect user");
		}

		var result = await _mediator.Send(new RemoveFavouritePropositionRequest(propositionId, userId));
		return result.Match<IActionResult>(
			success => Ok(),
			userNotFound => Unauthorized("User wasn't found"),
			propositionNotFound => BadRequest("Proposition wasn't found"));
	}

	/// <summary>
	/// Получение избранных предложений для текущего пользователя
	/// </summary>
	[Authorize]
	[HttpGet("favourite")]
	[ProducesResponseType(typeof(CollectionModel<PropositionModel>), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetFavouriteList()
	{
		var id = User.GetId();

		if (id is not { } userId)
		{
			return Unauthorized("Incorrect user");
		}

		var result = await _mediator.Send(new GetUserFavouritePropositionsRequest(userId));

		return result.Match<IActionResult>(
			userNotfound => Ok(new CollectionModel<PropositionModel>(Array.Empty<PropositionModel>())),
			Ok);
	}

	/// <summary>
	/// Получение самых популярных предложений
	/// </summary>
	[HttpGet("most-popular")]
	[ProducesResponseType(typeof(CollectionModel<PropositionStatisticModel>), StatusCodes.Status200OK)]
	public async Task<IActionResult> GetMostPopularCompanies([FromQuery] GetMostPopularPropositionsRequest request)
	{
		var result = await _mediator.Send(request);

		return Ok(result);
	}
}
