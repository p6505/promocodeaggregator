using NUnit.Framework;
using PromocodeAggregator.Api.Tests.Abstractions;
using PromocodeAggregator.Client.Models.Companies;

namespace PromocodeAggregator.Api.Tests;

[TestFixture]
[Ignore("Only for local testing")]
public class CompaniesControllerTest : TestBase
{
	[Test]
	public async Task CanGetCompanies()
	{
		var companies = await CreateClient().GetCompaniesList(null, null, 0, 20);

		Assert.IsNotEmpty(companies?.Items ?? Array.Empty<CompanyModel>());
	}

	[Test]
	public async Task CanGetCompaniesWithPagination()
	{
		var client = CreateClient();
		var companies = await client.GetCompaniesList(null, null, 0, 20);

		var secondRequestCount = Math.Max(0, companies.Items.Count - 1);
		var companies2 = await client.GetCompaniesList(null, null, 0, secondRequestCount);
		Assert.IsNotEmpty(companies?.Items ?? Array.Empty<CompanyModel>());
		Assert.IsNotEmpty(companies2?.Items ?? Array.Empty<CompanyModel>());
		Assert.That(companies2?.Items?.Count, Is.EqualTo(secondRequestCount));
	}

	[Test]
	public async Task CanGetCompaniesWithQuery()
	{
		var client = CreateClient();
		var companies = await client.GetCompaniesList(null, null, 0, 20);

		var company = companies.Items.First();
		var response = await client.GetCompaniesList(company.Name, null, 0, 20);
		Assert.IsNotEmpty(companies?.Items ?? Array.Empty<CompanyModel>());
		Assert.That(response?.Items?.Count, Is.EqualTo(1));
		Assert.That(company.Name, Is.EqualTo(response.Items.First().Name));
	}


	[Test]
	public async Task CanGetMostPopular()
	{
		var propositions = await CreateClient().GetMostPopularCompanies(0, 20);

		Assert.NotNull(propositions);
	}
}
