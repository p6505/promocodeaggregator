﻿using NUnit.Framework;
using PromocodeAggregator.Api.Tests.Abstractions;
using PromocodeAggregator.Client.Models;

namespace PromocodeAggregator.Api.Tests;

[TestFixture]
[Ignore("Only for local testing")]
public class CategoriesControllerTests : TestBase
{
	[Test]
	public async Task CanGetCategories()
	{
		var categories = await CreateClient().GetCategoriesList();

		Assert.IsNotEmpty(categories?.Items ?? Array.Empty<CategoryModel>());
	}

	[Test]
	public async Task CanGetCompaniesWithPagination()
	{
		var client = CreateClient();
		var categories = await client.GetCategoriesList();

		var category = categories.Items.First();

		var category2 = await client.GetCategoryById(category.Id);
		Assert.IsNotEmpty(categories?.Items ?? Array.Empty<CategoryModel>());
		Assert.NotNull(category2);
		Assert.That(category2.Id, Is.EqualTo(category.Id));
	}
}
