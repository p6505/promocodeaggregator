﻿using NUnit.Framework;
using PromocodeAggregator.Client;
using RestEase;

namespace PromocodeAggregator.Api.Tests.Abstractions;

[Parallelizable(ParallelScope.All)]
[FixtureLifeCycle(LifeCycle.InstancePerTestCase)]
[TestFixture]
public abstract class TestBase
{
	protected IPropositionsAggregatorClient CreateClient()
	{
		var client = new HttpClient();
		client.BaseAddress = new Uri("http://localhost:10000");
		return RestClient.For<IPropositionsAggregatorClient>(client);
	}
}
