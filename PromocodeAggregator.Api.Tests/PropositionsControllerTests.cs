﻿using NUnit.Framework;
using PromocodeAggregator.Api.Tests.Abstractions;

namespace PromocodeAggregator.Api.Tests;

[TestFixture]
[Ignore("For local tests only")]
public class PropositionsControllerTests : TestBase
{
	[Test]
	public async Task CanGetCompanyCodes()
	{
		var companies = await CreateClient().GetCompaniesList(null, null, 0, 20);
		var promocodes = await CreateClient().GetPropositions(companies.Items.First().Id, null, 0, 20);

		Assert.True(promocodes?.Items.Any());
	}

	[Test]
	public async Task CanGetCodeDetails()
	{
		var companies = await CreateClient().GetCompaniesList(null, null, 0, 20);
		var propostions = await CreateClient().GetPropositions(companies.Items.First().Id, null, 0, 20);
		var firstProposition = propostions.Items.First();

		var proposition = await CreateClient().GetProposition(firstProposition.Id);

		Assert.True(propostions?.Items.Any());
		Assert.NotNull(proposition);

		Assert.That(proposition.Id, Is.EqualTo(firstProposition.Id));
	}

	[Test]
	public async Task CanGetMostPopular()
	{
		var propositions = await CreateClient().GetMostPopularPropositions(0, 20);

		Assert.NotNull(propositions);
	}
}
